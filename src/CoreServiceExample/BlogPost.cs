﻿using System;
using System.Collections.Generic;

namespace CoreServiceExample
{
    public class BlogPost
    {
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Content { get; set; }
        public string Keywords { get; set; }
        public DateTime Created { get; set; }
        public string Author { get; set; }
        public List<string> Tags { get; set; }
    }
}
