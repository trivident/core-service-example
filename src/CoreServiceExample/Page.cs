﻿namespace CoreServiceExample
{
    public class Page
    {
        public string Title { get; set; }
        public string FileName { get; set; }
    }
}
