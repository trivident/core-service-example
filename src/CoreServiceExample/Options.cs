﻿using CommandLine;

namespace CoreServiceSynchronization
{
    public class Options
    {
        [Option('u', "username", Required = true, HelpText = "Tridion Username")]
        public string Username { get; set; }

        [Option('p', "password", Required = true, HelpText = "Tridion Password")]
        public string Password { get; set; }

        [Option('d', "domain", Required = false, HelpText = "Tridion Domain")]
        public string Domain { get; set; }

        [Option('h', "hostname", Required = true, HelpText = "Tridion Hostname")]
        public string Hostname { get; set; }
    }

    [Verb("create-schema", HelpText = "Create schema")]
    public class SchemaOptions : Options
    {
        [Option('f', "folder", Required = true, HelpText = "Tridion folder id")]
        public string FolderId { get; set; }
    }

    [Verb("create-components", HelpText = "Create n components")]
    public class ComponentsOptions : Options
    {
        [Option('f', "folder", Required = true, HelpText = "Tridion folder id")]
        public string FolderId { get; set; }

        [Option('s', "schema", Required = true, HelpText = "Tridion schema id")]
        public string SchemaId { get; set; }

        [Option('n', "amount", Required = true, HelpText = "Amount of number of components to generate")]
        public int Amount { get; set; }

        [Option('l', "language", Default = "en", Required = false, HelpText = "Specify the language")]
        public string Language { get; set; }
    }

    [Verb("create-pages", HelpText = "Create n pages")]
    public class PagesOptions : Options
    {
        [Option('f', "folder", Required = true, HelpText = "Tridion folder id")]
        public string FolderId { get; set; }

        [Option('n', "amount", Required = true, HelpText = "Amount of number of components to generate")]
        public int Amount { get; set; }

        [Option(Required = false, HelpText = "Publish created pages to these purposes, seperated by a ';' ")]
        public string Purposes { get; set; }

        [Option(Required = false, Default = false, HelpText = "Publish created pages?")]
        public bool Publish { get; set; }

        [Option('l', "language", Default = "en", Required = false, HelpText = "Specify the language")]
        public string Language { get; set; }
    }
}
