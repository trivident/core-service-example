﻿using Bogus;
using CommandLine;
using CoreServiceSynchronization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Xml;
using Tridion.ContentManager.CoreService.Client;

namespace CoreServiceExample
{
    class Program
    {
        static void Main(string[] args)
        {
            Parser.Default.ParseArguments<SchemaOptions, ComponentsOptions, PagesOptions>(args)
                .WithParsed((SchemaOptions opts) => CreateSchema(opts))
                .WithParsed((ComponentsOptions opts) => CreateComponents(opts))
                .WithParsed((PagesOptions opts) => CreatePages(opts))
                .WithNotParsed((errs) => HandleParseError(errs));
        }

        static int CreateSchema(SchemaOptions options)
        {
            using (var client = GetClient(options.Hostname, options.Username, options.Password, options.Domain, CoreServiceInstance.SdlWeb8))
            {
                try
                {
                    var schema = client.GetDefaultData(ItemType.Schema, options.FolderId, new ReadOptions()) as SchemaData;
                    schema.Title = "Blogpost";
                    schema.Description = "Fake blogposts";
                    schema.RootElementName = "blogpost";
                    schema.Xsd = "<xsd:schema xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"urn:blogpost\" xmlns:tcmi=\"http://www.tridion.com/ContentManager/5.0/Instance\" elementFormDefault=\"qualified\" targetNamespace=\"urn:blogpost\"> <xsd:import namespace=\"http://www.tridion.com/ContentManager/5.0/Instance\"></xsd:import> <xsd:annotation> <xsd:appinfo> <tcm:Labels xmlns:tcm=\"http://www.tridion.com/ContentManager/5.0\"> <tcm:Label ElementName=\"title\" Metadata=\"false\">Title</tcm:Label> <tcm:Label ElementName=\"summary\" Metadata=\"false\">Summary</tcm:Label> <tcm:Label ElementName=\"content\" Metadata=\"false\">Content</tcm:Label> <tcm:Label ElementName=\"keywords\" Metadata=\"false\">Keywords</tcm:Label> <tcm:Label ElementName=\"created\" Metadata=\"false\">Created</tcm:Label> <tcm:Label ElementName=\"author\" Metadata=\"false\">Author</tcm:Label> <tcm:Label ElementName=\"tags\" Metadata=\"false\">Tags</tcm:Label> </tcm:Labels> </xsd:appinfo> </xsd:annotation> <xsd:element name=\"blogpost\"> <xsd:complexType> <xsd:sequence> <xsd:element name=\"title\" minOccurs=\"1\" maxOccurs=\"1\" type=\"xsd:normalizedString\"> <xsd:annotation> <xsd:appinfo> <tcm:ExtensionXml xmlns:tcm=\"http://www.tridion.com/ContentManager/5.0\"></tcm:ExtensionXml> </xsd:appinfo> </xsd:annotation> </xsd:element> <xsd:element name=\"summary\" minOccurs=\"1\" maxOccurs=\"1\" type=\"xsd:normalizedString\"> <xsd:annotation> <xsd:appinfo> <tcm:ExtensionXml xmlns:tcm=\"http://www.tridion.com/ContentManager/5.0\"></tcm:ExtensionXml> </xsd:appinfo> </xsd:annotation> </xsd:element> <xsd:element name=\"content\" minOccurs=\"1\" maxOccurs=\"1\" type=\"tcmi:XHTML\"> <xsd:annotation> <xsd:appinfo> <tcm:ExtensionXml xmlns:tcm=\"http://www.tridion.com/ContentManager/5.0\"></tcm:ExtensionXml> <tcm:Size xmlns:tcm=\"http://www.tridion.com/ContentManager/5.0\">5</tcm:Size> <tcm:FilterXSLT xmlns:tcm=\"http://www.tridion.com/ContentManager/5.0\"> <xsl:stylesheet xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" xmlns=\"\" version=\"1.0\"> <xsl:output omit-xml-declaration=\"yes\" method=\"xml\" cdata-section-elements=\"script\"></xsl:output> <xsl:template name=\"FormattingFeatures\"> <FormattingFeatures xmlns=\"http://www.tridion.com/ContentManager/5.2/FormatArea\"> <Doctype>Transitional</Doctype> <AccessibilityLevel>0</AccessibilityLevel> <DisallowedActions></DisallowedActions> <DisallowedStyles></DisallowedStyles> </FormattingFeatures> </xsl:template> <xsl:template match=\"/ | node() | @*\"> <xsl:copy> <xsl:apply-templates select=\"node() | @*\"></xsl:apply-templates> </xsl:copy> </xsl:template> <xsl:template match=\"*[ (self::br or self::p or self::div) and normalize-space(translate(., &apos; &apos;, &apos;&apos;)) = &apos;&apos; and not(@*) and not(processing-instruction()) and not(comment()) and not(*[not(self::br) or @* or * or node()]) and not(following::node()[not( (self::text() or self::br or self::p or self::div) and normalize-space(translate(., &apos; &apos;, &apos;&apos;)) = &apos;&apos; and not(@*) and not(processing-instruction()) and not(comment()) and not(*[not(self::br) or @* or * or node()]) )]) ]\"> <!-- ignore all paragraphs and line-breaks at the end that have nothing but (non-breaking) spaces and line breaks --> </xsl:template> <xsl:template match=\"br[parent::div and not(preceding-sibling::node()) and not(following-sibling::node())]\"> <!-- Chrome generates <div><br/></div>. Renders differently in different browsers. Replace it with a non-breaking space --> <xsl:text> </xsl:text> </xsl:template> </xsl:stylesheet> </tcm:FilterXSLT> </xsd:appinfo> </xsd:annotation> </xsd:element> <xsd:element name=\"keywords\" minOccurs=\"1\" maxOccurs=\"1\" type=\"xsd:normalizedString\"> <xsd:annotation> <xsd:appinfo> <tcm:ExtensionXml xmlns:tcm=\"http://www.tridion.com/ContentManager/5.0\"></tcm:ExtensionXml> </xsd:appinfo> </xsd:annotation> </xsd:element> <xsd:element name=\"created\" minOccurs=\"1\" maxOccurs=\"1\" type=\"xsd:dateTime\"> <xsd:annotation> <xsd:appinfo> <tcm:ExtensionXml xmlns:tcm=\"http://www.tridion.com/ContentManager/5.0\"></tcm:ExtensionXml> </xsd:appinfo> </xsd:annotation> </xsd:element> <xsd:element name=\"author\" minOccurs=\"1\" maxOccurs=\"1\" type=\"xsd:normalizedString\"> <xsd:annotation> <xsd:appinfo> <tcm:ExtensionXml xmlns:tcm=\"http://www.tridion.com/ContentManager/5.0\"></tcm:ExtensionXml> </xsd:appinfo> </xsd:annotation> </xsd:element> <xsd:element name=\"tags\" minOccurs=\"1\" maxOccurs=\"unbounded\" type=\"xsd:normalizedString\"> <xsd:annotation> <xsd:appinfo> <tcm:ExtensionXml xmlns:tcm=\"http://www.tridion.com/ContentManager/5.0\"></tcm:ExtensionXml> </xsd:appinfo> </xsd:annotation> </xsd:element> </xsd:sequence> </xsd:complexType> </xsd:element> </xsd:schema>";

                    client.Create(schema, null);
                }
                catch (FaultException ex)
                {
                    Console.WriteLine($"Couldnt create schema in folder location {options.FolderId}, because of exception:{ex}");
                    return 1;
                }
            }
            return 0;
        }

        static int CreatePages(PagesOptions options)
        {
            using (var client = GetClient(options.Hostname, options.Username, options.Password, options.Domain, CoreServiceInstance.SdlWeb8))
            {
                var lorem = new Bogus.DataSets.Lorem(locale: options.Language);

                var fakerPages = new Faker<Page>()
                    .RuleFor(bp => bp.Title, f => lorem.Sentence(10));

                var pages = fakerPages.GenerateLazy(options.Amount);

                var publishTcmIds = new List<string>();
                foreach (var fakepage in pages)
                {
                    try
                    {
                        var page = client.GetDefaultData(ItemType.Page, options.FolderId, null) as PageData;
                        page.Title = fakepage.Title;
                        page.FileName = fakepage.Title.Replace(' ', '-');

                        var p = client.Create(page, new ReadOptions());
                        publishTcmIds.Add(p.Id);
                    }
                    catch (FaultException ex)
                    {
                        Console.WriteLine($"Couldnt create schema in folder location {options.FolderId}, because of exception:{ex}");
                        return 1;
                    }
                }

                if (options.Publish)
                {
                    var publishInstruction = new PublishInstructionData {
                        RenderInstruction = new RenderInstructionData(),
                        ResolveInstruction = new ResolveInstructionData()
                    };

                    client.Publish(publishTcmIds.ToArray(), publishInstruction, options.Purposes.Split(';'), PublishPriority.Low, null);
                }

            }
            return 0;
        }

        static int CreateComponents(ComponentsOptions options)
        {
            using (var client = GetClient(options.Hostname, options.Username, options.Password, options.Domain, CoreServiceInstance.SdlWeb8))
            {
                var lorem = new Bogus.DataSets.Lorem(locale: options.Language);

                var testBlogPosts = new Faker<BlogPost>()
                    .RuleFor(bp => bp.Content, f => lorem.Paragraphs())
                    .RuleFor(bp => bp.Created, f => f.Date.Past())
                    .RuleFor(bp => bp.Summary, f => lorem.Sentences(3))
                    .RuleFor(bp => bp.Keywords, f => string.Join(", ", lorem.Words()))
                    .RuleFor(bp => bp.Title, f => lorem.Sentence(10))
                    .RuleFor(bp => bp.Author, f => f.Name.FullName())
                    .RuleFor(bp => bp.Tags, f => new List<string> { f.Hacker.Noun(), f.Hacker.Noun(), f.Hacker.Noun(), });

                var blogPosts = testBlogPosts.GenerateLazy(options.Amount);
                foreach (var blogPost in blogPosts)
                {
                    var tags = "";
                    foreach (var tag in blogPost.Tags)
                    {
                        tags += $"<tags>{tag}</tags>";
                    }
                    var content = $"<blogpost xmlns=\"urn:blogpost\"><title>{blogPost.Title}</title><summary>{blogPost.Summary}</summary><content>{blogPost.Content}</content><keywords>{blogPost.Keywords}</keywords><created>{blogPost.Created.ToString("yyyy-MM-ddTHH:mm:ss")}</created><author>{blogPost.Author}</author>{tags}</blogpost>";

                    try
                    {
                        ComponentData component = client.GetDefaultData(ItemType.Component, options.FolderId, new ReadOptions()) as ComponentData;
                        component.Title = blogPost.Title;
                        component.Content = content;
                        component.Schema = new LinkToSchemaData { IdRef = options.SchemaId };
                        client.Create(component, null);
                    }
                    catch (FaultException ex)
                    {
                        Console.WriteLine($"Couldnt create component(s) because of exception {ex}");
                        return 1;
                    }

                }
            }
            return 0;
        }

        static void HandleParseError(IEnumerable<Error> errs)
        {
            Console.ReadLine();
        }

        static IEnumerable<string> GetComponents(string path)
        {
            if (File.Exists(path))
            {
                return File.ReadAllLines(path);
            }
            return Enumerable.Empty<string>();
        }

        //static IEnumerable<string> GetComponents(Options options)
        //{
        //    using (var client = GetClient(options.Username, ...))
        //    {
        //        var filter = new UsingItemsFilterData
        //        {
        //            ItemTypes = new[] { ItemType.Component }
        //        };
        //        var l = client.GetList("tcm:1-234-8", filter);
        //        foreach (var item in l)
        //        {
        //            var component = item as ComponentData;
        //        }

        //        client.ReadAsync("tcm:...", new ReadOptions());
        //    }
        //}


        static SessionAwareCoreServiceClient GetClient(string hostName, string username, string password, string domain, CoreServiceInstance version)
        {
            var netTcpBinding = new NetTcpBinding
            {
                MaxReceivedMessageSize = 2147483647,
                ReaderQuotas = new XmlDictionaryReaderQuotas
                {
                    MaxStringContentLength = 2147483647,
                    MaxArrayLength = 2147483647
                }
            };

            var remoteAddress =
                new EndpointAddress($"net.tcp://{hostName}:2660/CoreService/{(int)version}/netTcp");

            var coreServiceClient = new SessionAwareCoreServiceClient(netTcpBinding, remoteAddress);

            if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
            {
                coreServiceClient.ClientCredentials.Windows.ClientCredential.UserName = username;
                coreServiceClient.ClientCredentials.Windows.ClientCredential.Password = password;
            }

            if (!string.IsNullOrEmpty(domain))
            {
                coreServiceClient.ClientCredentials.Windows.ClientCredential.Domain = domain;
            }

            return coreServiceClient;
        }

        enum CoreServiceInstance
        {
            Tridion2011 = 2011,
            Tridion2013 = 2013,
            SdlWeb8 = 201501
        }
    }
}
